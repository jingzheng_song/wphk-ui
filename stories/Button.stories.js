import { FitButton } from '@';
import { rem } from '@tencent/ppd-base-libs';
rem();;
export default {
  title: 'Example/Button',
  component: FitButton,
  argTypes: {
    type: { control: { type: 'select', options: ['primary', 'warn', 'default', 'disabled', 'default-disabled'] } },
    size: { control: { type: 'select', options: ['normal', 'small'] } },
    click: { action: 'button clicked ' },
  },
};

const Template = (args, { argTypes }) => ({
  data: () => ({}),
  props: Object.keys(argTypes),
  components: { FitButton },
  template: '<FitButton @click="click" v-bind="$props">按钮</FitButton>',
});

export const Primary = Template.bind({});
Primary.args = {
  type: 'primary',
};

export const Default = Template.bind({});
Default.args = {
  type: 'default',
};

export const Warn = Template.bind({});
Warn.args = {
  type: 'warn',
};

export const Disabled = Template.bind({});
Disabled.args = {
  type: 'disabled',
};

export const DefaultDisabled = Template.bind({});
DefaultDisabled.args = {
  type: 'default-disabled',
};

const wrapMultiComp = template => `
  <div style="display: flex; flex-direction: column; justify-content: center; align-items: center;">
  ${template}
  </div>
  `;
export const 主按钮 = () => ({
  components: { FitButton },
  template: wrapMultiComp('<FitButton type="primary">正常态</FitButton> \
    <FitButton type="primary">点击态</FitButton> \
    <FitButton type="disabled">等待/不可点击</FitButton> \
    <FitButton type="primary">一二三四五六七八九</FitButton> \
    <FitButton type="primary">一二三四五六七八九十</FitButton> \
    '),
});

export const 次按钮 = () => ({
  components: { FitButton },
  template: wrapMultiComp('<FitButton type="default">弱化按钮</FitButton> \
    <FitButton type="default">点击态</FitButton> \
    <FitButton type="default-disabled">失效按钮</FitButton> \
    <FitButton type="warn">警示按钮</FitButton> \
    <FitButton type="default-disabled">失效按钮</FitButton> \
    '),
});

export const 小按钮 = () => ({
  components: { FitButton },
  template: wrapMultiComp(`
    <FitButton type="primary" size="small">完成</FitButton> \
    <FitButton type="disabled" size="small">完成</FitButton> \
    <FitButton type="default" size="small">完成</FitButton> \
    <FitButton type="default-disabled" size="small">完成</FitButton> \
    <FitButton size="small" color="red">完成</FitButton> \
    <FitButton type="warn" size="small">完成</FitButton> \
  `),
});
