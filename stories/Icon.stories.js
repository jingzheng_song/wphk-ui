import { FitIcon } from '@';
import { rem } from '@tencent/ppd-base-libs';
rem();;
export default {
  title: 'Example/Icon',
  component: FitIcon,
  argTypes: {
    icon: { control: { type: 'select', options: ['tick', 'wechatPay', 'reload', 'security', 'verification', 'success-default-circle', 'close-default-circle'] } },
    width: { 
        control: { type: 'text' },
    },
    height: { 
        control: { type: 'text' },
    },
    fill: {
        control: { type: 'text' },
    }
    // size: { control: { type: 'select', options: ['', 'small'] } },
  },
};

const Template = (args, { argTypes }) => ({
  data: () => ({}),
  props: Object.keys(argTypes),
  components: { FitIcon },
  template: '<FitIcon v-bind="$props">按钮</FitIcon>',
});

export const Tick = Template.bind({});
Tick.args = {
  icon: 'tick',
};

export const WechatPay = Template.bind({});
WechatPay.args = {
  icon: 'wechatPay',
};

export const Reload = Template.bind({});
Reload.args = {
  icon: 'reload',
};

export const Security = Template.bind({});
Security.args = {
  icon: 'security',
};

export const Verification = Template.bind({});
Verification.args = {
  icon: 'verification',
};

export const SuccessDefaultCircle = Template.bind({});
SuccessDefaultCircle.args = {
  icon: 'success-default-circle',
};

const wrapMultiComp = template => `
  <div style="display: flex; flex-direction: column; justify-content: center; align-items: center;">
  ${template}
  </div>
  `;
export const Icon = () => ({
  components: { FitIcon },
  template: wrapMultiComp('<FitIcon icon="success">Success</FitIcon> \
    <FitIcon icon="warn">Warn</FitIcon> \
    <FitIcon icon="safeWarn">SafeWarn</FitIcon> \
    <FitIcon icon="waiting">Waiting</FitIcon> \
    <FitIcon icon="info">Info</FitIcon> \
    <FitIcon icon="clear">Clear</FitIcon> \
    <FitIcon icon="tick">Tick</FitIcon> \
    <FitIcon icon="close">Close</FitIcon> \
    <FitIcon icon="more">More</FitIcon> \
    <FitIcon icon="spinner">Spinner</FitIcon> \
    <FitIcon icon="closeDefaultCircle">CloseDefaultCircle</FitIcon> \
    <FitIcon icon="successDefaultCircle">Success-circle</FitIcon> \
    <FitIcon icon="loading">Loading</FitIcon> \
    '),
});

