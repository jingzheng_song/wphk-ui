import FitButton from './packages/button';
import FitInput from './packages/input';
import FitVerifyPwdPage from './packages/verify-pwd-page';
import FitDialog from './packages/dialog';
import FitForm from './packages/form';
import FitFormItem from './packages/form-item';
import FitHalfScreenDialog from './packages/half-screen-dialog';
import FitHalfScreenOption from './packages/half-screen-option';
import FitPicker from './packages/picker';
import FitToast from './packages/toast';
import FitIcon from './packages/icon';
import FitSelect from './packages/select';
import FitLayout from './packages/layout';
import FitCorner from './packages/corner';
import FitDatePicker from './packages/date-picker';
import FitResult from './packages/result';
import FitLoadMore from './packages/load-more';
import FitNation from './packages/nation';
import FitCheckbox from './packages/checkbox';
import FitBg from './packages/bg';
import FitTest from './packages/test';
import FitNavbar from './packages/navbar';
import FitNavbarItem from './packages/navbar-item';
import FitSwitch from './packages/switch';
import FitNumberBox from './packages/number-box';
import FitCard from './packages/card';
import FitTag from './packages/tag';
import FitSelectItem from './packages/select/src/select-item.vue';
import FitPassward from './packages/passward'
import FitKeyboard from './packages/keyboard'
import FitTips from './packages/tips'
import FitWxPhonex from './packages/wx-phonex'
import FitInfoBox from './packages/info-box';
import FitInfoBoxItem from './packages/info-box/src/item.vue';
import FitOperationBanner from './packages/operation-banner';
import FitBadge from './packages/badge';
import FitMsg from './packages/msg'
const compoentsMap = {
    FitIcon,
    FitInput,
    FitButton,
    FitVerifyPwdPage,
    FitDialog,
    FitForm,
    FitFormItem,
    FitHalfScreenDialog,
    FitHalfScreenOption,
    FitPicker,
    FitToast,
    FitSelect,
    FitLayout,
    FitCorner,
    FitDatePicker,
    FitResult,
    FitLoadMore,
    FitNation,
    FitCheckbox,
    FitBg,
    FitTest,
    FitNavbar,
    FitNavbarItem,
    FitSwitch,
    FitNumberBox,
    FitCard,
    FitTag,
    FitSelectItem,
    FitPassward,
    FitKeyboard,
    FitTips,
    FitWxPhonex,
    FitInfoBox,
    FitInfoBoxItem,
    FitOperationBanner,
    FitBadge,
    FitMsg
};

function install(Vue: any) {
    Object.keys(compoentsMap).forEach((conponentName) => {
        Vue.component(conponentName, compoentsMap[conponentName].options);
    });
}

export default Object.assign({ install }, compoentsMap);

export {
    FitIcon, FitInput, FitButton, FitVerifyPwdPage, FitDialog, FitForm, FitFormItem, FitHalfScreenDialog, FitHalfScreenOption, FitToast, FitSelect, FitLayout, FitCorner, FitDatePicker, FitPicker, FitResult, FitLoadMore, FitNation, FitCheckbox, FitBg, FitTest, FitNavbar, FitNavbarItem, FitSwitch, FitNumberBox, FitCard, FitTag, FitSelectItem, FitPassward, FitKeyboard,FitTips,FitWxPhonex,FitInfoBox,FitInfoBoxItem,FitOperationBanner,FitBadge,FitMsg
};
