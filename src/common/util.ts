export function dispatch(componentName, eventName: any, params: any[]) {
    // @ts-ignore
    let parent = this.$parent || this.$root;
    let name = parent.$options.name;

    while (parent && (!name || name !== componentName)) {
        parent = parent.$parent;

        if (parent) {
            name = parent.$options.name;
        }
    }
    if (parent) {
        // @ts-ignore
        parent.$emit.apply(parent, [eventName].concat(params));
    }
}
export function broadcast(componentName, eventName, params) {
    // @ts-ignore
    broadcast.call(this, componentName, eventName, params);
}

export function getPropByPath(obj, path, strict = false) {
    let tempObj = obj;
    path = path.replace(/\[(\w+)\]/g, '.$1');
    path = path.replace(/^\./, '');

    let keyArr = path.split('.');
    let i = 0;
    for (let len = keyArr.length; i < len - 1; ++i) {
        if (!tempObj && !strict) break;
        let key = keyArr[i];
        if (key in tempObj) {
            tempObj = tempObj[key];
        } else {
            if (strict) {
                throw new Error('please transfer a valid prop path to form item!');
            }
            break;
        }
    }
    return {
        o: tempObj,
        k: keyArr[i],
        v: tempObj ? tempObj[keyArr[i]] : null
    };
};

export function objectAssign(target, ...arg) {
    for (let i = 1, j = arguments.length; i < j; i++) {
        let source = arguments[i] || {};
        for (let prop in source) {
            if (source.hasOwnProperty(prop)) {
                let value = source[prop];
                if (value !== undefined) {
                    target[prop] = value;
                }
            }
        }
    }

    return target;
};


// 获取操作系统
export const getOsType = () => {
    const ua = navigator.userAgent.toLowerCase();
    const matches = ua.match(/iphone|ipad|android/g);
    if (matches && matches.length) {
        return matches[0];
    } else {
        return "other";
    }
};
