import { Component, Prop, Vue } from 'vue-property-decorator';
import { dispatch, broadcast } from "@/common/util";

@Component({
})
export default class Emitter extends Vue {
    dispatch(componentName, eventName: any, params: any) {
        dispatch.call(this, componentName, eventName, params);
    }
    broadcast(componentName, eventName, params) {
        broadcast.call(this, componentName, eventName, params);
    }
}
