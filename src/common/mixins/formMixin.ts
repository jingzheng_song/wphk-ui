import { Component, Prop, Vue } from 'vue-property-decorator';
import { dispatch, broadcast } from "@/common/util";

@Component({
})
export default class FormMixin extends Vue {
    @Prop({ default: true }) validateEvent;
    public isInForm = this.checkIsInForm();
    public checkIsInForm() {
        let parent = this.$parent;
        while (parent) {
            // @ts-ignore
            if (parent.$options.name !== 'FitFormItem') {
                parent = parent.$parent;
            } else {
                return true;
            }
        }
        return false;
    }
    notify(trigger: string, val) {
        if (this.validateEvent) {
            console.log('touch---->notify',`form.${trigger}`)
            this.dispatch('FitFormItem', `form.${trigger}`, [val]);
        }
    }
    dispatch(componentName, eventName: any, params: any) {
        dispatch.call(this, componentName, eventName, params);
    }
    broadcast(componentName, eventName, params) {
        broadcast.call(this, componentName, eventName, params);
    }
}
