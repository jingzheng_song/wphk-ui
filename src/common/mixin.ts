export default {
    props: {
        // 是否隐藏
        hidden: {
            type: Boolean,
            default: false,
        },
        // 是否禁用
        disabled: {
            type: Boolean,
            default: false,
        },
    },
};
