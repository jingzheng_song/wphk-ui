import Vue from 'vue'
import svgIcon from '@/packages/svgIcon' // svg组件
// 注册为全局组件
Vue.component('svg-icon', svgIcon)
const req = require.context('./svg', false, /\.svg$/)
const requireAll = requireContext => requireContext.keys().map(requireContext)
requireAll(req)