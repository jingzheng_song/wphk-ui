# 概述

fit-ui、fit-comopnents 用于香港钱包业务，在未稳定版本前 采用 substree 的方式引入。稳定后会采用 npm 包进行管理。

# 如何做贡献

可参考 src/fit-ui/packages/button 目录结构，从该目录拷贝出副本，并修改成你要的组件名字。并在@/index.ts 显示引入你的组件。
然后你就可以在项目中通过 import {xxx} from '@' 进行引用;  
之后开发完毕后 请在该仓库发起 pr 到 master 并关联仓库管理者 dddzhang 进行合并
**_代码修改原则为向下兼容 请谨慎修改已有代码_**

# 如何更新

在根目录保存提交后执行 npm run pull:ui 即可。

# 如何引入

新项目均已在脚手架接入 只需要 pw init 便能跑起。旧项目请按照以下指引进行引入.

# 如果

# 旧项目引入：

1.在项目根目录新增 install.js，引入如下代码：
注意 依赖安装使用 yarn 如果没有使用 yarn 的话 请先配置 yarn

```$xslt
yarn config set registry http://r.tnpm.oa.com
yarn config set proxy http://127.0.0.1:12679
```

```
const { exec } = require("child_process");
const path = require('path');
async function execShell(shellJs) {
    return new Promise((res, rej) => {
        console.log(`执行${shellJs}`)
        exec(shellJs, (error, stdout, stderr) => {
            if (error) {
                console.error(`执行的错误: ${error}`);
                rej(error);
                return;
            }
            console.log(`stdout: ${stdout}`);
            console.error(`stderr: ${stderr}`);
            res(stdout);
        })
    })
}

async function main() {
    await addSubstree();
    await addScript();
    await installDependence();
    await changeBabelrc();
    await changeTsConfig();
    await changeTslint();
}

main();

//安装依赖
async function installDependence() {
    return execShell(`yarn add @tencent/babel-plugin-component --dev && yarn add vue-property-decorator async-validator --save`)
}

async function addSubstree(){
    return execShell(`git subtree add --prefix=src/fit-components http://git.code.oa.com/ppd-ui-comp/fit-components.git master --squash && git subtree add --prefix=src/fit-ui http://git.code.oa.com/ppd-ui-comp/fit-ui-v2.git master --squash`)
}

async function changeBabelrc(){
    return execShell(`echo '{
        "presets": [
          "@vue/babel-preset-app"
        ],
        "plugins": [
          [
            "@tencent/babel-plugin-component",
            {
              "libraryName": "@",
              "libDir": "packages",
              "styleLibrary": {
                "name": "style",
                "base": false,
                "path": "[module].less"
              }
            }
          ]
        ]
      }'>.babelrc`)
}


async function changeTsConfig(){
    return execShell(`echo '{
        "compilerOptions": {
          "target": "esnext",
          "module": "esnext",
          "strict": true,
          "jsx": "preserve",
          "importHelpers": true,
          "moduleResolution": "node",
          "esModuleInterop": true,
          "allowSyntheticDefaultImports": true,
          "sourceMap": true,
          "noImplicitAny": false,
          "traceResolution": true,
          "baseUrl": ".",
          "allowJs": true,
          "checkJs": false,
          "experimentalDecorators": true,
          "resolveJsonModule":true,
          "types": [
            "webpack-env",
            "jest"
          ],
          "paths": {
            "@/*": [
              "src/*"
            ]
          },
          "lib": [
            "esnext",
            "dom",
            "dom.iterable",
            "scripthost"
          ]
        },
        "include": [
          "src/**/*.ts",
          "src/**/*.tsx",
          "src/**/*.vue",
          "tests/**/*.ts",
          "tests/**/*.tsx"
        ],
        "exclude": [
          "node_modules"
        ]
      }
      '>tsconfig.json`)
}


async function changeTslint(){
    return execShell(`echo '{
        "defaultSeverity": "warning",
        "extends": [
          "tslint:recommended"
        ],
        "linterOptions": {
          "exclude": [
            "node_modules/**",
            "src/fit-components/**"
          ]
        },
        "rules": {
          "quotemark": [true, "single"],
          "indent": [true, "spaces", 2],
          "interface-name": false,
          "ordered-imports": false,
          "object-literal-sort-keys": false,
          "no-consecutive-blank-lines": false,
          "no-console": false
        }
      }
      '>tslint.json`)
}


async function addScript() {
    const packageJson = require(path.resolve(process.cwd(),'./package.json'));
    packageJson.scripts['pull:ui'] = "git subtree pull --prefix=src/fit-ui http://git.code.oa.com/ppd-ui-comp/fit-ui-v2.git master --squash";
    packageJson.scripts['push:ui'] = "git subtree push --prefix=src/fit-ui http://git.code.oa.com/ppd-ui-comp/fit-ui-v2.git master --squash";
    packageJson.scripts['pull:components'] = "git subtree pull --prefix=src/fit-components http://git.code.oa.com/ppd-ui-comp/fit-components.git master --squash";
    packageJson.scripts['push:components'] = "git subtree push --prefix=src/fit-components http://git.code.oa.com/ppd-ui-comp/fit-components.git master --squash";
    const outputData = JSON.stringify(JSON.stringify(packageJson));
    return execShell(`echo ${outputData}>./package.json`)
}

```

2。由于 gitsubtree 要求 workspace 必须没有待提交的文件 所以请确保你的本地已经 commit 然后执行 node install 3.在 vue.config.js 的 configureWebpack 追加配置：此处用于指定主题

```$xslt
resolve: {
    alias: {
        themes: path.resolve(__dirname, 'src/fit-ui/packages/style/theme/default.less'),
    }
},
```

4.如果在 app.vue 中引入了@/style/main.less 请删除
