## Button 按钮

常用的操作按钮。

### 基础用法

基础的按钮用法。

:::demo 使用`type`、`plain`、`round`和`circle`属性来定义 Button 的样式。

```html
<div style="margin-top: 20px">
  <div>
    <fit-button type="primary">主按钮</fit-button>
  </div>
  <div>
    <fit-button type="default">次按钮</fit-button>
  </div>
  <div>
    <fit-button type="disabled">不可用按钮</fit-button>
  </div>
  <div>
    <fit-button type="warn">警告按钮</fit-button>
  </div>
  <h1>两种尺寸的按钮</h1>
  <div>
    <fit-button size="small">默认按钮</fit-button>
    <fit-button size="normal">默认按钮</fit-button>
  </div>
</div>
```

:::

### Attributes

| 参数     | 说明         | 类型    | 可选值                             | 默认值  |
| -------- | ------------ | ------- | ---------------------------------- | ------- |
| type     | 类型         | string  | primary / default / disabled/ warn | primary |
| size     | 尺寸         | string  | small / normal                     | normal  |
| disabled | 是否禁用     | boolean | -                                  | false   |
| color    | 传入背景颜色 | string  | -                                  | ''      |

### Input Events

| 事件名称 | 说明     | 回调参数 |
| -------- | -------- | -------- |
| click    | 点击事件 | event    |
