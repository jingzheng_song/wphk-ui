
# halfScreen


:::demo 使用`typels `、`plain`、`round`和`circle`属性来定义 Button 的样式。

```html
    <FitButton @click="show=true" style="margin-top:20px">打开弹窗</FitButton>
 <FitHalfScreenDialog :visible.sync="show" @onClose="show=false" :title='base.title' :subTitle='base.subTitle' :pin='base.pin' :pwdLength='6' @filled="filled">
    <span>测试</span>
    <br>
    <br>
</FitHalfScreenDialog>
 <script>
   export default {
     data() {
       return {
           show:false,
         base:{
           title:'12213',
           subTitle:'123123123'
         }
       };
     },
     methods:{
       filled(value){
         console.log(value)
       }
     }
   }
 </script>
```
:::
