## Select
常用的操作按钮。

### 基础用法

基础的按钮用法。

:::demo 使用`type`、`plain`、`round`和`circle`属性来定义 Button 的样式。

```html
<div class="fit-container">
    <FitSelect :items="list" v-model="curValue" placeholder="请选择" title="请选择"
                               confirmBtnText="确定"
                               ></FitSelect>
</div>
    
    <script>
       export default {
         data() {
           return {
               curValue:1,
                list:[{
                      label: '飞机票',
                      value: 0
                  }, {
                      label: '火车票',
                      value: 1
                  }]
           };
         }
       }
     </script>
  
```
:::
