
# icon


:::demo 使用`type`、`plain`、`round`和`circle`属性来定义 Button 的样式。111
```html
    <div class="icon-group">
        <div v-for="(item,index) in list" :key="index">
            <div class="icon-box">
                <fit-icon :icon="item"></fit-icon>
            </div>
            <span>{{item}}</span>
        </div>
    </div>
    <script>
        export default {
            data(){
                return {
                    list:["verification","camera","reload","security","tick","triangle","loading","circle", "download", "info", "safe-success", "safe-warn", "success", "success-circle", "success-no-circle", "waiting", "waiting-circle", "warn", "info-circle", "cancel", "search", "clear", "back", "delete","success"]
                }
            }
        }
    </script>
```
:::

### Attributes
| 参数      | 说明    | 类型      | 可选值       | 默认值   |
|---------- |-------- |---------- |-------------  |-------- |
| icon     | 图标   | string  |   verification / camera / reload / security / tick / triangle / loading / circle / download / info / safe-success / safe-warn / success / success-circle / success-no-circle / waiting / waiting-circle / warn / info-circle / cancel / search / clear / back / delete / success       |    -     |
