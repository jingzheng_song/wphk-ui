import { Component, Vue } from 'vue-property-decorator';

@Component
export default class IconMixin extends Vue {
    pxToRem(px) {
        return px / 200 + 'rem'
    }
}
