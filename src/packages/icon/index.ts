// import Component from './src/index.vue';
// //@ts-ignore
// // const requireAll = requireContext => {
// //     console.log('requireContext',requireContext)
// //     return requireContext.keys().map(requireContext)
// // }
// // const req = require.context('@/assets/icon', false, /\.svg$/)
// // requireAll(req)
// export default Component;


import Vue, { CreateElement, VNode } from 'vue';
import Icon from './src/index.vue';
import Tick from './src/tick.vue';
import Security from './src/security.vue';
import Camera from './src/camera.vue';
import Reload from './src/reload.vue';
import Verification from './src/verification.vue';
import WechatPay from './src/wechatPay.vue';
import SuccessDefaultCircle from './src/successDefaultCircle.vue';
import Close from './src/close.vue';
import More from './src/more.vue';
import CloseDefaultCircle from './src/closeDefaultCircle.vue';
import SafeWarn from './src/safeWarn.vue';
import Spinner from './src/spinner.vue';
const svgMap = {
    tick: Tick,
    security: Security,
    camera: Camera,
    reload: Reload,
    verification: Verification,
    wechatPay: WechatPay,
    successDefaultCircle: SuccessDefaultCircle,
    close: Close,
    more: More,
    closeDefaultCircle: CloseDefaultCircle,
    safeWarn: SafeWarn,
    spinner: Spinner
};
export default Vue.extend({
    name: 'FitIcon',
    props: {
        icon: { // 弹出框标题
            type: String,
            default: 'info',
        },
        width: {
            type: String,
            default: '48'
        },
        height: {
            type: String,
            default: '48'
        },
        fill: {
            type: String,
            default: '#000'
        },
        size: {
            type: [String, Number],
        },
    },
    render(createElement: CreateElement): VNode {
        // @ts-ignore
        if (svgMap[this.icon]) {
            // @ts-ignore
            return createElement(svgMap[this.icon], {
                props: {
                    fill: this.fill,
                    size: this.size,
                    width: this.width,
                    height: this.height,
                },
                class: {
                    [`fit-icon-${this.icon}`]: true
                }
            });
        }
        return createElement(Icon, {
            props: {
                fill: this.fill,
                icon: this.icon,
                width: this.width,
                height: this.height,
            },
            class: {
                [`fit-icon-${this.icon}`]: true
            }
        });
    },
});
