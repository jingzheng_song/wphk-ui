## Result
常用的操作按钮。

### 基础用法


:::demo 使用`type`、`plain`、`round`和`circle`属性来定义 Button 的样式。

```html
<div>
    <fit-result title="个人信息" desc="請選擇國籍地區並上傳證件照片">
        
    </fit-result>
</div>
    
  
```
:::


