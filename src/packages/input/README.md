## Input
常用的操作按钮。

### 基础用法

基础的按钮用法。

:::demo 使用`type`、`plain`、`round`和`circle`属性来定义 Button 的样式。

```html
<div>
    <fit-form title="input" desc="以下列出常用input选项">
        <fit-form-item label="text">
            <fit-input v-model="input"  placeholder="请输入内容"></fit-input>
        </fit-form-item>
        <fit-form-item label="textarea">
            <fit-input v-model="input" type="textarea" placeholder="请输入内容"></fit-input>
        </fit-form-item>
        <fit-form-item label="password">
            <fit-input v-model="input"  :showPassword="true" placeholder="请输入内容"></fit-input>
        </fit-form-item>
        <fit-form-item label="过滤输入数字">
            <fit-input v-model="input" :pattern="/^\d*$/" placeholder="请输入内容"></fit-input>
        </fit-form-item>
    </fit-form>
    
</div>


<script>
export default {
  data() {
    return {
      input: '',
    }
  }
}
</script>
  
```
:::
### Attributes
| 参数      | 说明    | 类型      | 可选值       | 默认值   |
|---------- |-------- |---------- |-------------  |-------- |
| showPassword     | 展示密码   | boolean  |   -       |    false     |
| disabled     | 是否禁用   | boolean  |   -       |    false     |
| readonly     | 是否只读   | boolean  |   -       |    false     |
| type     | 类型   | string  |   text / textarea       |    text     |
| autocomplete     | 自动完成表单   | string  |   on / off       |    off     |
| value     | 数值   | string | number  |   -      |         |
| pattern     | 输入过滤   | RegExp | number  |   -      |         |



### Input Events
| 事件名称      | 说明    | 回调参数   |
|---------- |-------- |-------- |
| focus     | 获取焦点   |  event  |
| blur     | 失去焦点   |  event  |

