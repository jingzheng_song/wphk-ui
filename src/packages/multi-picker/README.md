## Picker

常用的操作按钮。

### 基础用法

基础的按钮用法。

:::demo 使用`type`、`plain`、`round`和`circle`属性来定义 Button 的样式。

```html
<div>
  <fit-button @click="show=true" style="margin-top: 20px"
    >打开picker</fit-button
  >
  <fit-multi-picker
    v-model="value"
    :visible="show"
    :items="list"
    confirmBtnText="确定"
    @onConfirm="onConfirm"
    @onClose="show=false"
  ></fit-multi-picker>
</div>

<script>
  export default {
    data() {
      return {
        show: false,
        value: [0,0,1],
        list: [
          {
            label: "广东",
            value: "广东",
            children: [
              {
                label: "广州",
                value: 0,
                children: [
                  {
                    label: "海珠",
                    value: "海珠"
                  },
                  {
                    label: "番禺",
                    value: 1
                  }
                ]
              },
              {
                label: "佛山",
                value: 1,
                children: [
                  {
                    label: "禅城",
                    value: 0
                  },
                  {
                    label: "南海",
                    value: "南海"
                  }
                ]
              }
            ]
          },
          {
            label: "广西",
            value: 1,
            children: [
              {
                label: "南宁",
                value: 0,
                children: [
                  {
                    label: "青秀",
                    value: 0
                  },
                  {
                    label: "兴宁",
                    value: 1
                  }
                ]
              },
              {
                label: "桂林",
                value: 1,
                children: [
                  {
                    label: "象山",
                    value: 0
                  },
                  {
                    label: "秀峰",
                    value: 1
                  }
                ]
              }
            ]
          }
        ]
      };
    },
    methods: {
      onConfirm(item, index) {
        console.log(item, index);
        this.show = false;
      }
    }
  };
</script>
```
:::
