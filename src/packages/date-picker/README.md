## Date-Picker

### 基础用法

基础的Date-Picker用法。

:::demo 

```html
<div class="weui-container">
    <fit-form-item label="请选择时间" style="margin-top: 20px">
        <FitDatePicker placeholder="请选择出生日期" v-model="date" title="请选择出生日期" ></FitDatePicker>
    </fit-form-item>
    <span>时间：{{date}}</span>
</div>
    
    <script>
       export default {
         data() {
           return {
               date:'',
           };
         }
       }
     </script>
  
```
:::

### Attributes
| 参数      | 说明    | 类型      | 可选值       | 默认值   |
|---------- |-------- |---------- |-------------  |-------- |
| title     | 标题   | string  |   -       |         |
| placeholder     | 没有值时的显示   | string  |   -       |         |
| maskClose     | 点击阴影区域能否隐藏   | boolean  |   -       |   true      |
| confirmBtnText     | 点击确认按钮   | string  |   -       |     确定    |
| align     | 内容对齐方式   | left   |   -       |   -      |
| start     | 开始时间   | Date|string   |   -       |  1990/01/01     |
| defaultValue     | 默认值   | Date|string   |   -       |  2018/12/01    |
| end     | 结束时间   | Date|string   |   -       |  2018/12/01    |
| format     | 格式化   | string   |   -       |  DD-MM-YYYY    |
| outputformat     | 格式化   | string   |   -       |  DD-MM-YYYY    |
| previewformat     | 格式化   | string   |   -       |  DD-MM-YYYY    |
