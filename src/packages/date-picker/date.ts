import fecha from './fecha';
export function isDate(date: any) {
    if (date === null || date === undefined) { return false; }
    if (isNaN(new Date(date).getTime())) { return false; }
    if (Array.isArray(date)) { return false; } // deal with `new Date([ new Date() ]) -> new Date()`
    return true;
}

export const toDate = function (date: any) {
    return isDate(date) ? new Date(date) : null;
};

export const formatDate = function (date: any, format: string) {
    date = toDate(date);
    if (!date) { return ''; }
    return fecha.format(date, format || 'yyyy-MM-dd', '');
};

export const parseDate = function (string: string, format: string): Date {
    return fecha.parse(string, format || 'yyyy-MM-dd', '') as Date;
};

export const transformStringToTime = (str: string | Date, format: string): Date => {
    if (typeof str === 'string') {
        return parseDate(str, format);
        // return new Date(str.replace(/-/g, '/'));
    }
    return str;
};
