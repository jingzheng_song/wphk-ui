## 表单

### 基础用法


:::demo 使用`type`、`plain`、`round`和`circle`属性来定义 Button 的样式。

```html
<div>
    <fit-form title="个人信息" desc="請選擇國籍並上傳證件照片">
        <fit-form-item label="国籍地区">
            <span>中国香港</span>
            <span slot="error">填写错误</span>
            <span slot="suffix">更换国籍<fit-icon icon="triangle"></fit-icon></span>
        </fit-form-item>
        <fit-form-item label="上传身份证正面照片">
            <span slot="suffix" class="fit-color-light-blue">查看示例</span>
        </fit-form-item>
    </fit-form>
</div>
    <script>
       export default {
         data() {
           return {
               show:false,
                list:[{
                      label: '飞机票',
                      value: 0
                  }, {
                      label: '火车票(disabled)',
                      disabled: true,
                      value: 1
                  }, {
                      label: '的士票',
                      value: 2
                  }, {
                      label: '住宿费',
                      value: 3
                  }, {
                      label: '礼品费(disabled)',
                      value: 4,
                      disabled: true,
                  }, {
                      label: '活动费',
                      value: 5
                  }, {
                      label: '通讯费',
                      value: 6
                  }, {
                      label: '补助',
                      value: 7
                  }, {
                      label: '通讯费',
                      value: 8
                  }, {
                      label: '其他',
                      value: 9
                  }]
           };
         },
         methods:{
             onConfirm(item,index){
                 console.log(item,index)
                 this.show = false;
             }
         }
       }
     </script>
  
```
:::


