
# 验密组件


:::demo 使用`type`、`plain`、`round`和`circle`属性来定义 Button 的样式。

```html
 <fit-verify-pwd-page  :title='base.title' :subTitle='base.subTitle' :pin='base.pin' :pwdLength='6' @filled="filled"></fit-verify-pwd-page>
 <script>
   export default {
     data() {
       return {
         base:{
           title:'12213',
           subTitle:'123123123'
         }
       };
     },
     methods:{
       filled(value){
         console.log(value)
       }
     }
   }
 </script>
```
:::
