## Button 按钮
常用的操作按钮。

### 基础用法

基础的按钮用法。

:::demo 使用`type`、`plain`、`round`和`circle`属性来定义 Button 的样式。

```html
<div style="margin-top: 20px">
    <fit-button>测试默认按钮</fit-button>
  <fit-button type="disabled">测试默认按钮</fit-button>
  <fit-button type="default">测试默认按钮</fit-button>
  <fit-button type="warn">测试默认按钮</fit-button>
  <fit-button type="primary" >测试默认按钮</fit-button>
</div>
  
```
:::

