## Select
常用的操作按钮。

### 基础用法

基础的按钮用法。

:::demo 使用`type`、`plain`、`round`和`circle`属性来定义 Button 的样式。

```html
<div>
    <fit-button @click="show=true" style="margin-top: 20px">打开picker</fit-button>
    <fit-select v-model="curValue" :visible="show" @change="onChange" :items="list" confirmBtnText="确定" @onConfirm="onConfirm" @onClose="show=false"></fit-select>
</div>
    
    <script>
       export default {
         data() {
           return {
               show:false,
               curValue:1,
                list:[{
                      label: '飞机票',
                      value: 0
                  }, {
                      label: '火车票',
                      value: 1
                  }]
           };
         },
         methods:{
             onConfirm(item,index){
                 console.log(item,index)
                 this.show = false;
             },
             onChange(value){
                 console.log('捕获到变化',value)
             }
         }
       }
     </script>
  
```
:::
