## Picker
常用的操作按钮。

### 基础用法

基础的按钮用法。

:::demo 使用`type`、`plain`、`round`和`circle`属性来定义 Button 的样式。

```html
<div>
    <fit-button @click="show=true" style="margin-top: 20px">打开picker</fit-button>
    <fit-picker v-model="value" :visible="show" :items="list" confirmBtnText="确定" @onConfirm="onConfirm" @onClose="show=false"></fit-picker>
</div>
    
    <script>
       export default {
         data() {
           return {
               dateShow:false,
               show:false,
               value:3,
               dateData:[{"label":"2017年","value":2017,"children":[{"label":"1月","value":1,"children":[{"label":"1日","value":1},{"label":"8日","value":8},{"label":"15日","value":15},{"label":"22日","value":22},{"label":"29日","value":29}]},{"label":"3月","value":3,"children":[{"label":"5日","value":5},{"label":"12日","value":12},{"label":"19日","value":19},{"label":"26日","value":26}]},{"label":"5月","value":5,"children":[{"label":"7日","value":7},{"label":"14日","value":14},{"label":"21日","value":21},{"label":"28日","value":28}]},{"label":"7月","value":7,"children":[{"label":"2日","value":2},{"label":"9日","value":9},{"label":"16日","value":16},{"label":"23日","value":23},{"label":"30日","value":30}]},{"label":"9月","value":9,"children":[{"label":"3日","value":3},{"label":"10日","value":10},{"label":"17日","value":17},{"label":"24日","value":24}]},{"label":"11月","value":11,"children":[{"label":"5日","value":5},{"label":"12日","value":12},{"label":"19日","value":19},{"label":"26日","value":26}]}]},{"label":"2018年","value":2018,"children":[{"label":"1月","value":1,"children":[{"label":"7日","value":7},{"label":"14日","value":14},{"label":"21日","value":21},{"label":"28日","value":28}]},{"label":"3月","value":3,"children":[{"label":"4日","value":4},{"label":"11日","value":11},{"label":"18日","value":18},{"label":"25日","value":25}]},{"label":"5月","value":5,"children":[{"label":"6日","value":6},{"label":"13日","value":13},{"label":"20日","value":20},{"label":"27日","value":27}]},{"label":"7月","value":7,"children":[{"label":"1日","value":1},{"label":"8日","value":8},{"label":"15日","value":15},{"label":"22日","value":22},{"label":"29日","value":29}]},{"label":"9月","value":9,"children":[{"label":"2日","value":2},{"label":"9日","value":9},{"label":"16日","value":16},{"label":"23日","value":23},{"label":"30日","value":30}]},{"label":"11月","value":11,"children":[{"label":"4日","value":4},{"label":"11日","value":11},{"label":"18日","value":18},{"label":"25日","value":25}]}]},{"label":"2019年","value":2019,"children":[{"label":"1月","value":1,"children":[{"label":"6日","value":6},{"label":"13日","value":13},{"label":"20日","value":20},{"label":"27日","value":27}]},{"label":"3月","value":3,"children":[{"label":"3日","value":3},{"label":"10日","value":10},{"label":"17日","value":17},{"label":"24日","value":24},{"label":"31日","value":31}]},{"label":"5月","value":5,"children":[{"label":"5日","value":5},{"label":"12日","value":12},{"label":"19日","value":19},{"label":"26日","value":26}]},{"label":"7月","value":7,"children":[{"label":"7日","value":7},{"label":"14日","value":14},{"label":"21日","value":21},{"label":"28日","value":28}]},{"label":"9月","value":9,"children":[{"label":"1日","value":1},{"label":"8日","value":8},{"label":"15日","value":15},{"label":"22日","value":22},{"label":"29日","value":29}]},{"label":"11月","value":11,"children":[{"label":"3日","value":3},{"label":"10日","value":10},{"label":"17日","value":17},{"label":"24日","value":24}]}]},{"label":"2020年","value":2020,"children":[{"label":"1月","value":1,"children":[{"label":"5日","value":5},{"label":"12日","value":12},{"label":"19日","value":19},{"label":"26日","value":26}]},{"label":"3月","value":3,"children":[{"label":"1日","value":1},{"label":"8日","value":8},{"label":"15日","value":15},{"label":"22日","value":22},{"label":"29日","value":29}]},{"label":"5月","value":5,"children":[{"label":"3日","value":3},{"label":"10日","value":10},{"label":"17日","value":17},{"label":"24日","value":24},{"label":"31日","value":31}]},{"label":"7月","value":7,"children":[{"label":"5日","value":5},{"label":"12日","value":12},{"label":"19日","value":19},{"label":"26日","value":26}]},{"label":"9月","value":9,"children":[{"label":"6日","value":6},{"label":"13日","value":13},{"label":"20日","value":20},{"label":"27日","value":27}]},{"label":"11月","value":11,"children":[{"label":"1日","value":1},{"label":"8日","value":8},{"label":"15日","value":15},{"label":"22日","value":22},{"label":"29日","value":29}]}]},{"label":"2021年","value":2021,"children":[{"label":"1月","value":1,"children":[{"label":"3日","value":3},{"label":"10日","value":10},{"label":"17日","value":17},{"label":"24日","value":24},{"label":"31日","value":31}]},{"label":"3月","value":3,"children":[{"label":"7日","value":7},{"label":"14日","value":14},{"label":"21日","value":21},{"label":"28日","value":28}]},{"label":"5月","value":5,"children":[{"label":"2日","value":2},{"label":"9日","value":9},{"label":"16日","value":16},{"label":"23日","value":23},{"label":"30日","value":30}]},{"label":"7月","value":7,"children":[{"label":"4日","value":4},{"label":"11日","value":11},{"label":"18日","value":18},{"label":"25日","value":25}]},{"label":"9月","value":9,"children":[{"label":"5日","value":5},{"label":"12日","value":12},{"label":"19日","value":19},{"label":"26日","value":26}]},{"label":"11月","value":11,"children":[{"label":"7日","value":7},{"label":"14日","value":14},{"label":"21日","value":21},{"label":"28日","value":28}]}]},{"label":"2022年","value":2022,"children":[{"label":"1月","value":1,"children":[{"label":"2日","value":2},{"label":"9日","value":9},{"label":"16日","value":16},{"label":"23日","value":23},{"label":"30日","value":30}]},{"label":"3月","value":3,"children":[{"label":"6日","value":6},{"label":"13日","value":13},{"label":"20日","value":20},{"label":"27日","value":27}]},{"label":"5月","value":5,"children":[{"label":"1日","value":1},{"label":"8日","value":8},{"label":"15日","value":15},{"label":"22日","value":22},{"label":"29日","value":29}]},{"label":"7月","value":7,"children":[{"label":"3日","value":3},{"label":"10日","value":10},{"label":"17日","value":17},{"label":"24日","value":24},{"label":"31日","value":31}]},{"label":"9月","value":9,"children":[{"label":"4日","value":4},{"label":"11日","value":11},{"label":"18日","value":18},{"label":"25日","value":25}]},{"label":"11月","value":11,"children":[{"label":"6日","value":6},{"label":"13日","value":13},{"label":"20日","value":20},{"label":"27日","value":27}]}]},{"label":"2023年","value":2023,"children":[{"label":"1月","value":1,"children":[{"label":"1日","value":1},{"label":"8日","value":8},{"label":"15日","value":15},{"label":"22日","value":22},{"label":"29日","value":29}]},{"label":"3月","value":3,"children":[{"label":"5日","value":5},{"label":"12日","value":12},{"label":"19日","value":19},{"label":"26日","value":26}]},{"label":"5月","value":5,"children":[{"label":"7日","value":7},{"label":"14日","value":14},{"label":"21日","value":21},{"label":"28日","value":28}]},{"label":"7月","value":7,"children":[{"label":"2日","value":2},{"label":"9日","value":9},{"label":"16日","value":16},{"label":"23日","value":23},{"label":"30日","value":30}]},{"label":"9月","value":9,"children":[{"label":"3日","value":3},{"label":"10日","value":10},{"label":"17日","value":17},{"label":"24日","value":24}]},{"label":"11月","value":11,"children":[{"label":"5日","value":5},{"label":"12日","value":12},{"label":"19日","value":19},{"label":"26日","value":26}]}]},{"label":"2024年","value":2024,"children":[{"label":"1月","value":1,"children":[{"label":"7日","value":7},{"label":"14日","value":14},{"label":"21日","value":21},{"label":"28日","value":28}]},{"label":"3月","value":3,"children":[{"label":"3日","value":3},{"label":"10日","value":10},{"label":"17日","value":17},{"label":"24日","value":24},{"label":"31日","value":31}]},{"label":"5月","value":5,"children":[{"label":"5日","value":5},{"label":"12日","value":12},{"label":"19日","value":19},{"label":"26日","value":26}]},{"label":"7月","value":7,"children":[{"label":"7日","value":7},{"label":"14日","value":14},{"label":"21日","value":21},{"label":"28日","value":28}]},{"label":"9月","value":9,"children":[{"label":"1日","value":1},{"label":"8日","value":8},{"label":"15日","value":15},{"label":"22日","value":22},{"label":"29日","value":29}]},{"label":"11月","value":11,"children":[{"label":"3日","value":3},{"label":"10日","value":10},{"label":"17日","value":17},{"label":"24日","value":24}]}]},{"label":"2025年","value":2025,"children":[{"label":"1月","value":1,"children":[{"label":"5日","value":5},{"label":"12日","value":12},{"label":"19日","value":19},{"label":"26日","value":26}]},{"label":"3月","value":3,"children":[{"label":"2日","value":2},{"label":"9日","value":9},{"label":"16日","value":16},{"label":"23日","value":23},{"label":"30日","value":30}]},{"label":"5月","value":5,"children":[{"label":"4日","value":4},{"label":"11日","value":11},{"label":"18日","value":18},{"label":"25日","value":25}]},{"label":"7月","value":7,"children":[{"label":"6日","value":6},{"label":"13日","value":13},{"label":"20日","value":20},{"label":"27日","value":27}]},{"label":"9月","value":9,"children":[{"label":"7日","value":7},{"label":"14日","value":14},{"label":"21日","value":21},{"label":"28日","value":28}]},{"label":"11月","value":11,"children":[{"label":"2日","value":2},{"label":"9日","value":9},{"label":"16日","value":16},{"label":"23日","value":23},{"label":"30日","value":30}]}]},{"label":"2026年","value":2026,"children":[{"label":"1月","value":1,"children":[{"label":"4日","value":4},{"label":"11日","value":11},{"label":"18日","value":18},{"label":"25日","value":25}]},{"label":"3月","value":3,"children":[{"label":"1日","value":1},{"label":"8日","value":8},{"label":"15日","value":15},{"label":"22日","value":22},{"label":"29日","value":29}]},{"label":"5月","value":5,"children":[{"label":"3日","value":3},{"label":"10日","value":10},{"label":"17日","value":17},{"label":"24日","value":24},{"label":"31日","value":31}]},{"label":"7月","value":7,"children":[{"label":"5日","value":5},{"label":"12日","value":12},{"label":"19日","value":19},{"label":"26日","value":26}]},{"label":"9月","value":9,"children":[{"label":"6日","value":6},{"label":"13日","value":13},{"label":"20日","value":20},{"label":"27日","value":27}]},{"label":"11月","value":11,"children":[{"label":"1日","value":1},{"label":"8日","value":8},{"label":"15日","value":15},{"label":"22日","value":22},{"label":"29日","value":29}]}]},{"label":"2027年","value":2027,"children":[{"label":"1月","value":1,"children":[{"label":"3日","value":3},{"label":"10日","value":10},{"label":"17日","value":17},{"label":"24日","value":24},{"label":"31日","value":31}]},{"label":"3月","value":3,"children":[{"label":"7日","value":7},{"label":"14日","value":14},{"label":"21日","value":21},{"label":"28日","value":28}]},{"label":"5月","value":5,"children":[{"label":"2日","value":2},{"label":"9日","value":9},{"label":"16日","value":16},{"label":"23日","value":23},{"label":"30日","value":30}]},{"label":"7月","value":7,"children":[{"label":"4日","value":4},{"label":"11日","value":11},{"label":"18日","value":18},{"label":"25日","value":25}]},{"label":"9月","value":9,"children":[{"label":"5日","value":5},{"label":"12日","value":12},{"label":"19日","value":19},{"label":"26日","value":26}]},{"label":"11月","value":11,"children":[{"label":"7日","value":7},{"label":"14日","value":14},{"label":"21日","value":21},{"label":"28日","value":28}]}]},{"label":"2028年","value":2028,"children":[{"label":"1月","value":1,"children":[{"label":"2日","value":2},{"label":"9日","value":9},{"label":"16日","value":16},{"label":"23日","value":23},{"label":"30日","value":30}]},{"label":"3月","value":3,"children":[{"label":"5日","value":5},{"label":"12日","value":12},{"label":"19日","value":19},{"label":"26日","value":26}]},{"label":"5月","value":5,"children":[{"label":"7日","value":7},{"label":"14日","value":14},{"label":"21日","value":21},{"label":"28日","value":28}]},{"label":"7月","value":7,"children":[{"label":"2日","value":2},{"label":"9日","value":9},{"label":"16日","value":16},{"label":"23日","value":23},{"label":"30日","value":30}]},{"label":"9月","value":9,"children":[{"label":"3日","value":3},{"label":"10日","value":10},{"label":"17日","value":17},{"label":"24日","value":24}]},{"label":"11月","value":11,"children":[{"label":"5日","value":5},{"label":"12日","value":12},{"label":"19日","value":19},{"label":"26日","value":26}]}]},{"label":"2029年","value":2029,"children":[{"label":"1月","value":1,"children":[{"label":"7日","value":7},{"label":"14日","value":14},{"label":"21日","value":21},{"label":"28日","value":28}]},{"label":"3月","value":3,"children":[{"label":"4日","value":4},{"label":"11日","value":11},{"label":"18日","value":18},{"label":"25日","value":25}]},{"label":"5月","value":5,"children":[{"label":"6日","value":6},{"label":"13日","value":13},{"label":"20日","value":20},{"label":"27日","value":27}]},{"label":"7月","value":7,"children":[{"label":"1日","value":1},{"label":"8日","value":8},{"label":"15日","value":15},{"label":"22日","value":22},{"label":"29日","value":29}]},{"label":"9月","value":9,"children":[{"label":"2日","value":2},{"label":"9日","value":9},{"label":"16日","value":16},{"label":"23日","value":23},{"label":"30日","value":30}]},{"label":"11月","value":11,"children":[{"label":"4日","value":4},{"label":"11日","value":11},{"label":"18日","value":18},{"label":"25日","value":25}]}]},{"label":"2030年","value":2030,"children":[{"label":"1月","value":1,"children":[{"label":"6日","value":6},{"label":"13日","value":13},{"label":"20日","value":20},{"label":"27日","value":27}]},{"label":"3月","value":3,"children":[{"label":"3日","value":3},{"label":"10日","value":10},{"label":"17日","value":17},{"label":"24日","value":24},{"label":"31日","value":31}]},{"label":"5月","value":5,"children":[{"label":"5日","value":5},{"label":"12日","value":12},{"label":"19日","value":19},{"label":"26日","value":26}]},{"label":"7月","value":7,"children":[{"label":"7日","value":7},{"label":"14日","value":14},{"label":"21日","value":21},{"label":"28日","value":28}]},{"label":"9月","value":9,"children":[{"label":"1日","value":1},{"label":"8日","value":8},{"label":"15日","value":15},{"label":"22日","value":22},{"label":"29日","value":29}]},{"label":"11月","value":11,"children":[{"label":"3日","value":3},{"label":"10日","value":10},{"label":"17日","value":17},{"label":"24日","value":24}]}]}],
                list:[{
                      label: '飞机票',
                      value: 0
                  }, {
                      label: '火车票(disabled)',
                      disabled: true,
                      value: 1
                  }, {
                      label: '的士票',
                      value: 2
                  }, {
                      label: '住宿费',
                      value: 3
                  }, {
                      label: '礼品费(disabled)',
                      value: 4,
                      disabled: true,
                  }, {
                      label: '活动费',
                      value: 5
                  }, {
                      label: '通讯费',
                      value: 6
                  }, {
                      label: '补助',
                      value: 7
                  }, {
                      label: '通讯费',
                      value: 8
                  }, {
                      label: '其他',
                      value: 9
                  }]
           };
         },
         methods:{
             onConfirm(item,index){
                 console.log(item,index)
                 this.show = false;
             }
         }
       }
     </script>
  
```
:::



:::demo 使用`type`、`plain`、`round`和`circle`属性来定义 Button 的样式。

```html
<div>
    <fit-button @click="show=true" style="margin-top: 20px">打开picker</fit-button>
    <fit-picker :visible="show" :items="list" confirmBtnText="确定" @onConfirm="onConfirm" @onClose="show=false"></fit-picker>
</div>
    
    <script>
       export default {
         data() {
           return {
               show:false,
                list:[{
                      label: '飞机票',
                      value: 0
                  }, {
                      label: '火车票(disabled)',
                      value: 1
                  }]
           };
         },
         methods:{
             onConfirm(item,index){
                 console.log(item,index)
                 this.show = false;
             }
         }
       }
     </script>
  
```
:::
