## Picker
常用的操作按钮。

### 基础用法

基础的按钮用法。

:::demo 使用`type`、`plain`、`round`和`circle`属性来定义 Button 的样式。

```html
<div class="weui-container">
    <h1>asdhakshjdk </h1>
    <fit-nation placeholder="测试" v-model="country" :data="countryList"></fit-nation>
</div>
    
    <script>
       export default {
         data() {
           return {
               dateShow:false,
               date:"2015/03/01",
               countryList:[{label: '中国香港', value: 'HK'}, {label: '菲律宾', value: 'PH'}, {label: '中国', value: 'CN'}, {label: '中国澳门', value: 'MO'}, {label: '英国', value: 'GB'}, {label: '美国', value: 'US'}, {label: '澳大利亚', value: 'AU'}],
               country:''    
           };
         },
         methods:{
             onConfirm(time){
                 console.log(time)
             }
         }
       }
     </script>
  
```
:::

