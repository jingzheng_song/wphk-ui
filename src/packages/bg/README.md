
# bg
FitBg用于以下场景：  
1. 在不同的单页中需要不同颜色 
2. 需要隐藏下拉时的背后地址

:::demo 只需要指定color即可
```html
    <div>
        <FitBg :color="curColor"></FitBg>
        <h1>当前颜色：{{curColor}}</h1>
        <FitButton @click="addNum">点击切换背景</FitButton>
    </div>
    <script>
        export default {
            data(){
                return {
                  index:0,
                  colorList:['white','grey','black']
                }
            },
            methods:{
              addNum(){
                  this.index = (this.index+1)%this.colorList.length;
              }
            },
            computed:{
                curColor(){
                  return this.colorList[this.index];
                }
            }
        }
    </script>
```
:::


### Attributes
| 参数      | 说明    | 类型      | 可选值       | 默认值   |
|---------- |-------- |---------- |-------------  |-------- |
| color     | 颜色   | string  |   white / grey / 其他色值            |    #ffffff     |

