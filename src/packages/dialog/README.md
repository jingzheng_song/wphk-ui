## Dialog
常用的操作按钮。

### 基础用法

:::demo

```html
<div style="margin-top: 20px">
    <fit-button @click="openDialog1">点击显示dialog</fit-button>
    <fit-button @click="openDialog2">点击显示dialog（无标题）</fit-button>
    <fit-button @click="openDialog3">点击显示dialog（多个按钮）</fit-button>
    <fit-button @click="openDialog4">点击显示dialog（自定义内容）</fit-button>
    <fit-button @click="openDialog5">点击显示dialog（文本左对齐）</fit-button>
    <fit-button @click="openDialog6">点击显示dialog（多文本溢出）</fit-button>
    <fit-button @click="openDialog6">点击显示dialog（多文本溢出无标题）</fit-button>
    <fit-dialog v-show="show" :align="dialog.align" :title="dialog.title" :htmlContent="dialog.htmlContent"
                :content="dialog.content" :btns="dialog.btns"></fit-dialog>
</div>
<script>
  export default {
    data() {
      return {
        show: false,
        dialog: {}
      };
    },
    methods: {
      openDialog1() {
        this.show = true;
        this.dialog = {
          title: '弹窗标题',
          content: '弹窗内容，告知当前状态、信息和解决方法，描述文字尽量控制在三行内',
          btns: [{
            text: '取消',
            click: () => {
              this.show = false;
            }
          }, {
            text: '确定',
            click: () => {
              this.show = false;
            }
          }]
        };
      },
      openDialog2() {
        this.show = true;
        this.dialog = {
          content: '弹窗内容，告知当前状态、信息和解决方法，描述文字尽量控制在三行内',
          btns: [{
            text: '取消',
            click: () => {
              this.show = false;
            }
          }, {
            text: '确定',
            click: () => {
              this.show = false;
            }
          }]
        };
      },
      openDialog3() {
        this.show = true;
        this.dialog = {
          content: '弹窗内容，告知当前状态、信息和解决方法，描述文字尽量控制在三行内',
          btns: [{
            text: '确定',
            click: () => {
              this.show = false;
            }
          }, {
            text: '取消',
            click: () => {
              this.show = false;
            }
          }, {
            text: '我知道了',
            click: () => {
              this.show = false;
            }
          }]
        };
      },
      openDialog4() {
        this.show = true;
        this.dialog = {
          title: '标题',
          htmlContent: '弹窗内容<br>你可以看到换行了',
          btns: [{
            text: '确定',
            click: () => {
              this.show = false;
            }
          }]
        };
      },
      openDialog5() {
        this.show = true;
        this.dialog = {
          title: '标题',
          htmlContent: '弹窗内容<br>你可以看到换行了',
          align: 'left',
          btns: [{
            text: '确定',
            click: () => {
              this.show = false;
            }
          }]
        };
      },
      openDialog6() {
          this.show = true;
          this.dialog = {
            content: '内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多内容非常多多多',
            align: 'left',
            btns: [{
              text: '确定',
              click: () => {
                this.show = false;
              }
            }]
          };
        }
    }
  };
</script>
```
:::

### js调用
在项目中，可以通过js实现调用
在app.vue中引入FitDialog，在view中引入

```$xslt
import {FitDialog} from '@/fit-components'
```

然后在代码中调用：
```$xslt
import {showDialog,hideDialog} from '@/fit-components/dialog/main';
showDialog({
    title:'弹窗标题',
    content:'弹窗内容，告知当前状态、信息和解决方法，描述文字尽量控制在三行内',
    btns:[{
        text:'取消'
    },{
       text:'确定',
       click:() =>{
           hideDialog();
       }
   }]
})
```

### Attributes
| 参数      | 说明    | 类型      | 可选值       | 默认值   |
|---------- |-------- |---------- |-------------  |-------- |
| title     | 标题   | string  |   -       |         |
| content     | 描述   | string  |   -       |         |
| htmlContent     | 自定义内容   | string  |   -       |         |
| btns     | 按钮组   | btns[]  |   -       |         |
| align     | 内容对齐方式   | left   |   -       |   -      |

### btns
| 参数      | 说明    | 类型      | 可选值       | 默认值   |
|---------- |-------- |---------- |-------------  |-------- |
| text     | 按钮文本   | string  |   -       |     -    |
| click     | 点击事件   | function  |   -       |    -     |

