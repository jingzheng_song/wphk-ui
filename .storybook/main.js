// module.exports = {
//   stories: ["../src/**/*.stories.mdx", "../src/**/*.stories.@(js|jsx|ts|tsx)"],
//   addons: ["@storybook/addon-links", "@storybook/addon-essentials"],
//   webpackFinal: async (config, { configType }) => {
//     config.module.rules.push({
//       // this is for both less and scss
//       test: /.*\.(?:le|c|sc)ss$/,
//       loaders: [
//         "style-loader",
//         "css-loader",
//         "less-loader", // if scss then add 'sass-loader'
//       ],
//     });
//     // config.plugins.push(
//     //   new MiniCssExtractPlugin({
//     //     filename: "[name]-[contenthash].css",
//     //     chunkFilename: "[id]-[contenthash].css",
//     //   })
//     // );
//     return config;
//   },
//   // babel: async config => {
//   //   config.plugins.push([
//   //     '@tencent/babel-plugin-component',
//   //     {
//   //       libraryName: '@',
//   //       libDir: 'packages',
//   //       styleLibrary: {
//   //         name: 'style',
//   //         base: false,
//   //         path: '[module].less',
//   //       },
//   //     },
//   //   ])
//   //   return config
//   // },
// };
const path = require('path')

function resolve(str) {
  return path.join(__dirname, '../', str)
}

module.exports = {
  // stories: ['../src/**/*.stories.mdx', '../src/**/*.stories.@(js|jsx|ts|tsx)'],
  stories: ['../stories/**/*.stories.mdx', '../stories/**/*.stories.@(js|jsx|ts|tsx)'],
  addons: [
    '@storybook/addon-links',
    '@storybook/addon-essentials', 
    {
      name: '@storybook/addon-storysource',
      options: {
        loaderOptions: {
          injectStoryParameters: false,
        },
      },
    },
  ],
  webpackFinal: async config => {
    let alias = config.resolve.alias
    alias = {
      ...alias,
      '@': resolve('src'),
      themes: resolve('src/packages/theme-chalk/theme/default.less'),
      // themes: resolve('src/fit-ui/packages/style/theme/default.less'),
    }
    config.resolve.alias = alias;
    config.module.rules = config.module.rules.filter(
      f => f.test.toString() !== '/\\.css$/'
    );
    config.module.rules.push(
      {
        test:/\.(less|css)$/,
        use: [
          {
            loader: 'style-loader',
          },
          {
            loader: 'css-loader',
          },
          {
            loader: 'less-loader',
            // options: {
            //   javascriptEnabled: true,
            // },
          },
        ],
      },
    )
    return config
  },
  babel: async config => {
    config.plugins.push([
      '@tencent/babel-plugin-component',
      {
        libraryName: '@',
        libDir: 'packages',
        styleLibrary: {
          name: 'theme-chalk',
          base: false,
          path: '[module].less',
        },
      },
    ])
    return config
  },
}
