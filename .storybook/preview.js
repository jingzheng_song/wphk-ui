// export const parameters = {
//   actions: { argTypesRegex: "^on[A-Z].*" },
//   controls: {
//     matchers: {
//       color: /(background|color)$/i,
//       date: /Date$/,
//     },
//   },
// };

import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  viewport: {
    viewports: INITIAL_VIEWPORTS,
    defaultViewport: 'iphonex',
  },
  controls: { expanded: true },
}
export const globalTypes = {
  // theme: {
  //   name: 'Theme',
  //   description: '主题切换',
  //   defaultValue: 'HKWallet',
  //   toolbar: {
  //     icon: 'circlehollow',
  //     items: [
  //       { value: 'HKWallet', title: '香港钱包' },
  //       { value: 'remit', title: '跨境' },
  //     ],
  //   },
  // },
};