module.exports = {
  extends: [
    "@tencent/eslint-config-tencent",
    "@tencent/eslint-config-tencent/ts",
  ],
  plugins: ["vue", "@typescript-eslint"],
  parser: "vue-eslint-parser",
  parserOptions: {
    parser: "@typescript-eslint/parser",
    extraFileExtensions: [".vue"],
    createDefaultProgram: true,
  },
  rules: {},
};
